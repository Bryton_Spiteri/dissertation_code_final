import cv2
import imutils
import numpy as np
from collections import deque
from math import sqrt

vid = cv2.VideoCapture('Street2.MP4')

frameWidth, frameHeight = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH)), int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))

# vid = cv2.line(frame, (600, 630), (780, 900), (0,0,255), 3)

# calling all defines and creating the output
while (True):
    ret, frame = vid.read()

    frame = cv2.line(frame, (950, 575), (325, 1050), (0, 0, 255), 3)
    frame = cv2.line(frame, (325, 1050), (1150, 1050), (0, 0, 255), 3)
    frame = cv2.line(frame, (1150, 1050), (1600, 700), (0, 0, 255), 3)
    frame = cv2.line(frame, (1600, 700), (950, 575), (0, 0, 255), 3)

    x = 700
    y = 300
    # line 1
    if y <= (16 * x / 15) + 250:
        if y <= 1050:
            if y >= (11 * x / 15) - 343.3333:
                if y >= (-23 * x / 70) + 877.8571:
                    frame = cv2.circle(frame, (x, y), 20, (0, 0, 255), 3)
                else:
                    frame = cv2.circle(frame, (x, y), 20, (255, 0, 0), 3)

    cv2.putText(frame, "Person Counter: ", (50, 50),
                cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    cv2.putText(frame, "JayWalking Counter:", (50, 80),
                cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    cv2.imshow('frame', frame)
    if not ret:
        break

    # define q as the exit button
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

# release the video capture object
vid.release()

# Closes all the windows currently opened.
cv2.destroyAllWindows()
