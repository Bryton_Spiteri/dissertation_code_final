import numpy as np
import argparse
import imutils
import time
import cv2
import os
import csv

# Arguments needed to be passed to run code (Have defaults if not included)
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
                help="path to input video")
ap.add_argument("-s", "--street", required=True,
                help="street cam view")
ap.add_argument("-o", "--output", required=True,
                help="path to output video")
ap.add_argument("-y", "--yolo", required=True,
                help="base path to YOLO directory")
ap.add_argument("-c", "--confidence", type=float, default=0.6,
                help="minimum probability to filter weak detections")
ap.add_argument("-t", "--threshold", type=float, default=0.3,
                help="threshold when applying non-maxima suppression")
ap.add_argument("-f", "--csvfrms", type=str, default="csvfrms_1.csv",
                help="filename where object counts are stored in CVS format")
ap.add_argument("-r", "--csvroi", type=str, default="csvrois_1.csv",
                help="filename where frames with jaywalking objects are stored in CVS format")

args = vars(ap.parse_args())

# loading the COCO library
labelsPath = os.path.sep.join([args["yolo"], "coco.names"])
LABELS = open(labelsPath).read().strip().split("\n")

# having the full range to possible colours present
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3), dtype="uint8")
ALERT_COLOR = 0, 0, 255

# setting paths to access .weights and . cfg
weightsPath = os.path.sep.join([args["yolo"], "yolov3.weights"])
configPath = os.path.sep.join([args["yolo"], "yolov3.cfg"])

# Load the Yolo trained on COCO Dataset and select output layers
print("[Log] Perparing to run YOLO Detection...")
net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
ln = net.getLayerNames()
ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

# load inputted the video, point to output path, and frame size
vs = cv2.VideoCapture(args["input"])
writer = None
(W, H) = (None, None)

# open csv file to save object detection counts
fo = open(args["csvfrms"],"wt",newline="")
csv_writer = csv.writer(fo)

# open csv file to save jaywalking and ROI counts
froi = open(args["csvroi"],"wt",newline="")
csv_writer_roi = csv.writer(froi)

# open text file to log intermine results
logfile = args["csvfrms"]+"_log.txt"
fl = open(logfile,"wt")

# determining the total amount of frames in the input
try:
    prop = cv2.cv.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() \
        else cv2.CAP_PROP_FRAME_COUNT
    total = int(vs.get(prop))
    print("[Log] {} total number of frames in video".format(total))

# ERROR while trying to determine the number of frames in the input
except:
    print("[Log] could not determine # of frames in video")
    print("[Log] no approx. completion time can be provided")
    total = -1

# Setting Street View
street = args["street"]

# ROI_cnt   Number of ROI in the street
# p_cnt     Array of int. Size = ROI_cnt. Each element represents number of points/sides in ROI.
# px        Array of arrays. Size = ROI_cnt. Each element repesents the x-coordinate.
# py        Array of arrays. Size = ROI_cnt. Each element repesents the y-coordinate.
# p0        Array of arrays. Size = ROI_cnt. Each element repesents the inequlity operator for ROI test.

if street == "str1":
    # defining of arrays
    ROI_cnt = 5
    p_cnt = [4, 4, 5, 4, 4]
#    px = [[0, 170, 410, 150], [192, 750, 1900, 1150, 550, 435], [1450, 1580, 1900, 1905]]
#    py = [[220, 360, 320, 180], [392, 1050, 1050, 500, 400, 350], [400, 430, 380, 330]]
    px = [[0, 170, 410, 150], [192, 245, 550, 435], [245, 750, 1900, 1210, 550], [940, 1210, 1450, 1350], [1450, 1580, 1900, 1905]]
    py = [[220, 360, 320, 180], [392, 450, 400, 350], [450, 1050, 1050, 500, 400], [460, 500, 450, 420], [400, 430, 380, 330]]
    po = [["lt", "lt", "gt", "gt"], ["lt", "lt", "gt", "gt"], ["lt", "lt", "gt", "gt", "gt"], ["lt", "lt", "gt", "gt"], ["lt", "lt", "lt", "gt"]]
elif street == "str2":
    ROI_cnt = 5
    p_cnt = [4, 4, 4, 6, 4]
    px = [[0, 5, 430, 500], [625, 545, 753, 810], [660, 275, 1160, 1335], [810, 660, 1335, 1310, 1265,955],
          [1310, 1335, 1805, 1800]]
    py = [[500, 530, 590, 570], [585, 610, 650, 585], [750, 1045, 1040, 870], [585, 750, 870, 650, 590, 565],
          [645, 870, 780, 735]]
    po = [["lt", "lt", "lt", "gt"], ["gt", "lt", "lt", "gt"], ["gt", "lt", "lt", "gt"], ["gt", "lt", "gt", "gt", "gt", "gt"],
          ["lt", "lt", "gt", "gt"]]
else:
    print("ERROR: Unknown Street!!")
    quit()

# control variables
tot_frm = -1    # To control the number of frames to output, then stop. Set to -1 to do parse entire input video
frm_cnt = 0     # loop counter for frame count
test_ROI = 0    # value 1 to test ROI inequalities, used only to create and test first time new ROI is created

# initialise arrays for gradient and y-intercept

# create array with gradient of straight line graphs forming ROI
mx = []         # Array of array. To capture side gradients for all ROI
mx_tmp = []     # Array. To capture gradient for one ROI
j = 1
while j <= ROI_cnt:
    i = 1
    while i <= p_cnt[j-1]:
        # gradient is delta y / delta x. Use of mod (%) operator to use last point with first.
        # print(i, py[i - 1], m[i - 1], px[i - 1])
        mx_tmp.append((py[j-1][i - 1] - py[j-1][i % p_cnt[j-1]]) / (px[j-1][i - 1] - px[j-1][i % p_cnt[j-1]]))
        if i == p_cnt[j-1]:
            mx.append(mx_tmp)
            # print(mx_tmp)
            # print(mx)
            mx_tmp = []
            break
        i += 1
    if j == ROI_cnt:
        break
    j += 1

# create array with y-intercept of straight line graphs forming ROI
cx = []         # Array of array. To capture y-intercept for all ROI
cx_tmp = []     # Array. To capture y-interceptfor one ROI
j = 1
while j <= ROI_cnt:
    i = 1
    while i <= p_cnt[j-1]:
        cx_tmp.append(py[j-1][i - 1] - (mx[j-1][i - 1] * px[j-1][i - 1]))
        if i == p_cnt[j-1]:
            cx.append(cx_tmp)
            # print(cx_tmp)
            # print(cx)
            cx_tmp = []
            break
        i += 1
    if j == ROI_cnt:
        break
    j += 1

# build and save header row for CSV file by using array 'data'.
# element 0 will represent 'frame number'
# element 1 will represent 'jaywalk count'
# followed by the object 'labels'
data = []
data.append("frame")
data.append("jaywalk count")
for i in LABELS:
    data.append(i)
csv_writer.writerow(data)

data_ROI = []
data_ROI.append("Frame")
data_ROI.append("Roi")
data_ROI.append("X")
data_ROI.append("Y")
data_ROI.append("W")
data_ROI.append("H")
csv_writer_roi.writerow(data_ROI)
data_ROI_cnt = 0
start2 = time.time()

# loop over frames from the input
while True:

    # next frame
    (grabbed, frame) = vs.read()

    # increment frm_cnt and break when tot_frm reached.
    if frm_cnt == tot_frm:
        break
    frm_cnt += 1
    print("Frame Count = " + str(frm_cnt))
    l_line = "\nFrame Count = " + str(frm_cnt) + "\n"
    fl.write(l_line)
    l_line = "Frame"

    # if not grabbed = end
    if not grabbed:
        break

    # if dimensions empty grab them
    if W is None or H is None:
        (H, W) = frame.shape[:2]

    # creation of a blob from input and then pass to the YOLO object detector,
    # getting bounding boxes and probabilities of object
    blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416), swapRB=True, crop=False)
    net.setInput(blob)
    start = time.time()
    layerOutputs = net.forward(ln)
    end = time.time()

    # initialize list of bounding boxes, confidences, and class IDs
    boxes = []
    confidences = []
    classIDs = []

    # two arrays are initialised.
    # data will be used to output frame count and all object labels and ClassIDs found in frame
    # data = []
    # data.append("frame = ")
    # data.append(frm_cnt)
    # data_cnt will be used to output frame count and the number of objects encountered in the frame
    # the ClassIDs count will be added to data_cnt element offset by 2, as first two elements will be used to
    # capture frame count and jaywalk count
    data_cnt = [0,0,0,0,0,0,0,0,0,0,0,0]
    data_cnt[0] = frm_cnt
    data_ROI = [0,0,0,0,0,0]


    # loop over each of the layer outputs
    for output in layerOutputs:

        # loop over each of the detections
        for detection in output:

            # extract the class ID and confidence
            scores = detection[5:]
            classID = np.argmax(scores)
            confidence = scores[classID]

            # filter out weak predictions
            if confidence > args["confidence"]:

                # scaling the bounding box coordinates relative to the image, keeping in mind that YOLO
                box = detection[0:4] * np.array([W, H, W, H])
                (centerX, centerY, width, height) = box.astype("int")

                # The center coord used to derive the top
                # and left corner of bounding box
                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))

                # updating lists
                boxes.append([x, y, int(width), int(height)])
                confidences.append(float(confidence))
                classIDs.append(classID)

                # detected object class ID (coco.names)
                detObject = LABELS[classID]
                # print("Detected:[" + str(classID + 1) + "]" + detObject + " : at " + str(x) + "," + str(y))

                # if object detected is ClassID 1 (person) and bounding box not in defined
                # zebra cross region (we can define this specifically in a config file for example
                # then alert that person crossed road not on zebra cross

                if (classID == 0):
                    print("ALERT!: Person at:[" + str(classID + 1) + "]" + detObject + " : at " + str(x) + "," + str(y))

    # apply non-maxima suppression to remove overlapping
    idxs = cv2.dnn.NMSBoxes(boxes, confidences, args["confidence"],
                            args["threshold"])

    # making sure detection exists
    if len(idxs) > 0:

        persCount = 0
        jayCount = 0

        # draw ROIs
        j = 1
        while j <= ROI_cnt:
            i = 1
            while i <= p_cnt[j-1]:
                frame = cv2.line(frame, (px[j-1][i-1], py[j-1][i-1]), (px[j-1][i%p_cnt[j-1]], py[j-1][i%p_cnt[j-1]]), (0, 0, 255), 3)
                if i == p_cnt[j-1]:
                    break
                i += 1
            if j == ROI_cnt:
                break
            j += 1

        # loop over the indexes kept
        for i in idxs.flatten():
            # get the box coord
            (x, y) = (boxes[i][0], boxes[i][1])
            (w, h) = (boxes[i][2], boxes[i][3])

            # update arrays data and data_cnt
            # data.append(LABELS[classIDs[i]])
            # (LABELS[classIDs[i]])
            # data.append(classIDs[i])
            # print(classIDs[i])
            # if classID[i] is larger than 9, do not update data_cnt, as array only initialised to track 10 objects
            if classIDs[i] <= 9:
                data_cnt[classIDs[i]+2] += 1
            else:
                data_cnt.append("x=")
                data_cnt.append(classIDs[i])
            # print(data_cnt)

            # draw a box rectangle and label
            color = [int(c) for c in COLORS[classIDs[i]]]

            # Setting Object Base line
            x0 = int(x + (w / 2))
            y0 = int(y + (h))

            # Set ROI Test coordinates
            # if test_ROI == 1:
            #     x0 = 1350
            #     y0 = 800

            if classIDs[i] == 0:
                persCount = persCount + 1
                jay_flag = 0
                j = 1
                while j <= ROI_cnt:
                    jay_flag = 0
                    jay_flag_prev = 0
                    ix = 1
                    while ix <= p_cnt[j-1]:
                        # print("test = ", ix, po[j-1][ix - 1])
                        if po[j-1][ix-1] == 'gt':
                            if y0 >= (mx[j-1][ix-1] * x0) + cx[j-1][ix-1]:
                                jay_flag += 1
                        elif y0 <= (mx[j-1][ix-1] * x0) + cx[j-1][ix-1]:
                                jay_flag += 1

                        # print("test result ", "ROI_cnt = ", j, " - p_cnt = ", ix, " jay_flag is ", jay_flag)
                        l_line = "x = " + str(x0) + ", y = " + str(y0) + ", test result " + "ROI_cnt = " + str(j) + " - p_cnt = " + str(ix) + " jay_flag is " + str(jay_flag) +"\n"
                        fl.write(l_line)

                        if ix == p_cnt[j-1] or jay_flag == jay_flag_prev:
                            break
                        jay_flag_prev = jay_flag
                        ix += 1

                    # print ("jay_flag = ", jay_flag)
                    if jay_flag == p_cnt[j-1]:
                                color = ALERT_COLOR
                                print("Jaywalking Detected!!")
                                jayCount = jayCount + 1
                                text_alert = "Jaywalking Detected!"
                                cv2.putText(frame, text_alert, (x + 5, y0),
                                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
                                data_ROI_cnt += 1
                                data_ROI[0] = frm_cnt
                                data_ROI[1] = j
                                data_ROI[2] = str(x0)
                                data_ROI[3] = str(y0)
                                data_ROI[4] = w
                                data_ROI[5] = h
                                csv_writer_roi.writerow(data_ROI)

                    if j == ROI_cnt or jay_flag == p_cnt[j-1]:
                        # print ("end of ROI check = ", ROI_cnt, "jay_flag = ", jay_flag)
                        break
                    j += 1

            # if test_ROI == 1:
            #     cv2.circle(frame, (x0, y0), 10, ALERT_COLOR, 5)
            cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
            text = "{}: {:.4f}: {}: {}".format(LABELS[classIDs[i]],
                                       confidences[i], str(x), str(y)) ################# Chsned
            cv2.putText(frame, text, (x, y - 5),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    cv2.putText(frame, "Person Counter: " + str(persCount) , (50, 50),
                cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)
    cv2.putText(frame, "JayWalking Counter: " + str(jayCount), (50, 100),
                cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 2)
    cv2.putText(frame, "Frame Number: " + str(frm_cnt), (50, 150),
                cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0), 2)

    # check if video writer = None
    if writer is None:
        # initialize writer
        fourcc = cv2.VideoWriter_fourcc(*"MJPG")
        writer = cv2.VideoWriter(args["output"], fourcc, 30,
                                 (frame.shape[1], frame.shape[0]), True)

        # output info log on frames
        if total > 0:
            elap = (end - start)
            print("[Log] Single frame took {:.4f} seconds to process".format(elap))
            print("[Log] Estimated time to finish all the frames: {:.4f}".format(
                elap * total))

    # write output to disk
    writer.write(frame)
    data_cnt[1] = jayCount
    # csv_writer.writerow(data)
    csv_writer.writerow(data_cnt)

# release the file pointers
print("[Log] Sample Done, Video placed in Output folder...")
# this condition is to check whether frame writer has been opened, in case that frm_cnt is set to zero (0)
data_ROI = [0,0,0,0,0,0]
data_ROI[0] = args["output"]
data_ROI[1] = street
data_ROI[2] = total
data_ROI[3] = frm_cnt
data_ROI[4] = data_ROI_cnt
if data_ROI_cnt >0:
    data_ROI[5] = data_ROI_cnt / frm_cnt
else:
    data_ROI[5] = data_ROI_cnt
csv_writer_roi.writerow(data_ROI)

end2 = time.time()
elap2 = (end2 - start2)
print("[Log] Start Time is {:.4f}".format(start2))
print("[Log] End Time is {:.4f}".format(end2))
print("[Log] Estimated time to finish all the frames was: {:.4f}".format(elap * total))
print("[Log] Actual time to finish all the frames is: {:.4f}".format(elap2))

if writer is not None:
    writer.release()
vs.release()
fo.close()
froi.close()
fl.close()

# Command Args
# ROI_Yolo_Video.py --input C:\Users\mario\Documents\Bryton\MCAST\Thesis\Thesis_Working\Work.mp4 --street str1 --output C:\Users\mario\Documents\Bryton\MCAST\Thesis\Thesis_Working\output/Work-out1.avi --yolo C:\Users\mario\Documents\Bryton\MCAST\Thesis\Thesis_Working\yolo-coco

#ROI_Yolo_Video.py --input Sample_02.mp4 --street str1 --output output/Sample_out2f.avi --yolo yolo-coco --csvfrms output/test_2f.csv
#ROI_Yolo_Video.py --input Sample_01.mp4 --street str2 --output Sample_01.avi --yolo yolo-coco --csvfrms Test_1a.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_01.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_01.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_01.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_01.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_02.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_02.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_02.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_02.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_03.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_03.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_03.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_03.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_04.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_04.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_04.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_04.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_05.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_05.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_05.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_05.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_06.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_06.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_06.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_06.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_07.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_07.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_07.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_07.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_08.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_08.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_08.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_08.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_09.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_09.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_09.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_09.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_10.mp4 --street str1 --output E:\ThesisSamples\Output\Video\Sample_10.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_10.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_10.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_11.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_11.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_11.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_11.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_12.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_12.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_12.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_12.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_13.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_13.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_13.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_13.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_14.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_14.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_14.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_14.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_15.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_15.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_15.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_15.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_16.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_16.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_16.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_16.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_17.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_17.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_17.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_17.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_18.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_18.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_18.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_18.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_19.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_19.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_19.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_19.csv
#ROI_Yolo_Video.py --input E:\ThesisSamples\Sample_20.mp4 --street str2 --output E:\ThesisSamples\Output\Video\Sample_20.avi --yolo yolo-coco --csvfrms E:\ThesisSamples\Output\CSV\Frms_Sample_20.csv --csvroi E:\ThesisSamples\Output\CSV\ROI_Sample_20.csv